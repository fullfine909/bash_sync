#!/bin/bash

# Define paths
APPS_DIR=~/Apps
LOCAL_APPS_DIR=~/.local/share/applications
DOTS_CURSOR_DIR=~/.dots/cursor
CURSOR_CONFIG_DIR=~/.config/Cursor/User
BACKUP_DIR=~/.dots/backup/cursor

# Get current timestamp for backup directory
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

# Function to create symlink if it doesn't exist
create_symlink() {
    if [ ! -L "$1" ] || [ ! -e "$1" ]; then
        ln -sf "$2" "$1"
        echo "Created symlink: $1 -> $2"
    else
        echo "Symlink already exists: $1 -> $(readlink -f "$1")"
    fi
}

# Check and create Apps directory
if [ ! -d "$APPS_DIR" ]; then
    mkdir -p "$APPS_DIR"
    echo "Created $APPS_DIR directory"
else
    echo "$APPS_DIR directory already exists"
fi

# Check and create applications symlink
create_symlink "$APPS_DIR/applications" "$LOCAL_APPS_DIR"

# Copy .desktop file
cp "$DOTS_CURSOR_DIR/cursor.desktop" "$LOCAL_APPS_DIR/"
echo "Copied cursor.desktop to $LOCAL_APPS_DIR/"

# Copy SVG file
cp "$DOTS_CURSOR_DIR/cursor.svg" "$APPS_DIR/"
echo "Copied cursor.svg to $APPS_DIR/"

# Create Cursor config directory if it doesn't exist
if [ ! -d "$CURSOR_CONFIG_DIR" ]; then
    mkdir -p "$CURSOR_CONFIG_DIR"
    echo "Created $CURSOR_CONFIG_DIR directory"
else
    echo "$CURSOR_CONFIG_DIR directory already exists"
fi

# Backup existing config files
if [ -d "$CURSOR_CONFIG_DIR" ] && [ "$(ls -A $CURSOR_CONFIG_DIR)" ]; then
    mkdir -p "$BACKUP_DIR/$TIMESTAMP"
    mv "$CURSOR_CONFIG_DIR"/* "$BACKUP_DIR/$TIMESTAMP/"
    echo "Backed up existing config files to $BACKUP_DIR/$TIMESTAMP/"
else
    echo "No existing config files to backup"
fi

# Copy config files
if [ -d "$DOTS_CURSOR_DIR/config" ]; then
    cp -r "$DOTS_CURSOR_DIR/config/"* "$CURSOR_CONFIG_DIR/"
    echo "Copied config files from $DOTS_CURSOR_DIR/config/ to $CURSOR_CONFIG_DIR/"
else
    echo "Warning: $DOTS_CURSOR_DIR/config/ directory not found. No config files copied."
fi

echo "Cursor image app installation complete!"
