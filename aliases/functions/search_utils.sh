# ~/.dots/functions/search_meta_alias.sh

# ===================================
# Search Programs and Alias Functions
# ===================================

# Search Programs
cpi() {
    if [[ -z "$1" ]]; then
        echo -e "\e[31mError: Please provide a program name.\e[0m"
        return 1
    fi

    local program=$1

    # Check via DNF
    if command -v dnf &>/dev/null; then
        if dnf list installed "$program" &>/dev/null; then
            echo -e "\e[32m$program is installed via DNF.\e[0m"
        else
            echo -e "\e[33m$program is not installed via DNF.\e[0m"
        fi
    else
        echo "DNF is not available on this system."
    fi

    # Check via Snap
    if command -v snap &>/dev/null; then
        if snap list | grep -q "^$program "; then
            echo -e "\e[32m$program is installed via Snap.\e[0m"
        else
            echo -e "\e[33m$program is not installed via Snap.\e[0m"
        fi
    else
        echo "Snap is not available on this system."
    fi

    # Check via Flatpak
    if command -v flatpak &>/dev/null; then
        if flatpak list | grep -qi "$program"; then
            echo -e "\e[32m$program is installed via Flatpak.\e[0m"
        else
            echo -e "\e[33m$program is not installed via Flatpak.\e[0m"
        fi
    else
        echo "Flatpak is not available on this system."
    fi
}

# Find Alias
falias() {
    local alias_name="$1"
    local aliases_dir="$HOME/.dots/aliases"
    local functions_dir="$HOME/.dots/functions"
    local alias_found=false

    # Search in Aliases
    for file in "$aliases_dir"/*.sh; do
        [ -f "$file" ] || continue

        if grep -q "^alias $alias_name=" "$file"; then
            echo -e "\n-----------------------"
            echo "Alias found in: $file"
            echo "-----------------------"
            grep "^alias $alias_name=" "$file"
            alias_found=true
            break
        fi
    done

    # Search in Functions
    if [ "$alias_found" = false ]; then
        for file in "$functions_dir"/*.sh; do
            [ -f "$file" ] || continue

            if grep -q "^$alias_name()" "$file"; then
                echo -e "\n-----------------------"
                echo "Function found in: $file"
                echo "-----------------------"
                sed -n "/^$alias_name()/,/^}/p" "$file" | sed '$d'
                alias_found=true
                break
            fi
        done
    fi

    if [ "$alias_found" = false ]; then
        echo "Alias or function '$alias_name' not found in any of the files in '$aliases_dir' or '$functions_dir'."
    fi
}

