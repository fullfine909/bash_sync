#!/usr/bin/env bash

set -eou pipefail

kitty @ ls | $HOME/.config/kitty/kitty-convert-dump.py > $HOME/.config/kitty/session.conf

# Remove the following line:
# kitty @ send-text "kitty session dumped"

# Instead, you could use this to print to stdout:
echo "Kitty session dumped to $HOME/.config/kitty/session.conf"
