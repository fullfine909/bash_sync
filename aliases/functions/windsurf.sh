# ~/.dots/functions/extract_windsurf.sh

# ===================================
# Extract and Move Windsurf Function
# ===================================

ewin() {
    if [[ -z $1 ]]; then
        echo "Usage: extract_and_move_windsurf <path/to/Windsurf-linux-x64.tar.gz>"
        return 1
    fi

    local file_path="$1"
    local dest_dir="$HOME/Dev/Windsurf"

    # Check if file exists
    if [[ ! -f "$file_path" ]]; then
        echo "Error: File '$file_path' does not exist."
        return 1
    fi

    # Create destination directory if it doesn't exist
    mkdir -p "$dest_dir"

    # Extract tarball to the destination directory
    tar -xvzf "$file_path" -C "$dest_dir" --strip-components=1

    echo "Windsurf extracted to $dest_dir"
}

# Alias to run Windsurf
alias rwin="$HOME/Dev/Windsurf/windsurf"

