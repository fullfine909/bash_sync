" Basic Settings
colorscheme desert
set nocompatible              " Turn off compatibility with old-time Vi
filetype plugin indent on     " Enable filetype detection
syntax on                     " Enable syntax highlighting
set number                    " Show line numbers
set relativenumber            " Show relative line numbers
set cursorline                " Highlight the current line
set cursorcolumn              " Highlight the current column
set splitbelow                " Open horizontal splits below current window
set splitright                " Open vertical splits to the right of current window
set scrolloff=10              " Keep 10 lines visible above/below cursor during scrolling
set shiftwidth=4              " Number of spaces to use for each step of (auto)indent
set tabstop=4                 " Number of spaces that a <Tab> in the file counts for
set autoindent                " Copy indent from current line when starting a new line
set expandtab                 " Convert tabs to spaces
set showcmd                   " Display incomplete commands
set ruler                     " Show the cursor position all the time
set showmatch                 " Highlight matching braces
set hlsearch                  " Highlight search results
set history=8000              " Increase command history size
set backup                    " Enable backups
set backupdir=~/.vim/backup// " Directory for backups
set undofile                  " Enable persistent undo
set undodir=~/.vim/undo//     " Directory for undo history
set directory=/tmp            " Directory for swap files
set autoread                  " Automatically read file when changed from the outside
set hidden                    " Allow buffer switching without saving
set textwidth=80              " Set maximum width of text
set mousehide                 " Hide mouse when typing
set wildmenu                  " Enhanced command line completion
set wildmode=list:longest     " Completion mode settings
set wildignore=*.jpg,*.mp4,*.zip,*.iso,*.pdf,*.pyc,*.odt,*.png,*.gif,*.tar,*.gz,*.xz,*.bz2,*.tgz,*.db,*.exe,*.odt,*.xlsx,*.docx,*.tar,*.rar,*.img,*.odt,*.m4a,*.bmp,*.ogg,*.mp3,*.gzip,*.flv,*.deb,*.rpm " Pattern to ignore during file navigation

" Ensure settings are re-applied on buffer enter
autocmd BufEnter * setlocal formatoptions-=cro comments=

" Plugins
call plug#begin()
Plug 'preservim/nerdtree'     " NERDTree plugin for file navigation
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }  " Fzf for fuzzy file searching
Plug 'junegunn/fzf.vim'       " Vim integration for fzf
call plug#end()

" Clipboard Mappings
" Yank current line to system clipboard
noremap yy "+yy
" Yank to system clipboard
noremap y "+y
" Yank to the end of line to system clipboard
noremap Y "+Y

" Custom Mappings
" Define leader key as '-'
let mapleader = "-"           " Define local leader key as '\'
let maplocalleader = "\\"     " Toggle NERDTree with F5
nnoremap <F5> :NERDTreeToggle
" Clear search highlighting with \<leader>
nnoremap <silent> <localleader>\ :nohlsearch<cr>
" Exit insert mode with 'ii'
inoremap ii <esc>
" Open command line with space
nnoremap <space> :
" Select all text in buffer with <leader>a
noremap <leader>a ggVG
" Paste from clipboard with middle mouse button
nnoremap <mousemiddle> <esc>"*P
" Center screen on next search result
nnoremap n nzz
" Center screen on previous search result
nnoremap N Nzz
" Insert line below without entering insert mode
nnoremap <leader>o o<esc>
" Insert line above without entering insert mode
nnoremap <leader>O O<esc>
" Save and reload .vimrc
nnoremap <leader>s :w<CR>:source $MYVIMRC<cr>
" Choose font
nnoremap <leader>f :set guifont=*<cr>
" Toggle paste mode with F6
set pastetoggle=<F6>
" Toggle line numbers with F7
nnoremap <F7> :set number!<cr>
" Toggle relative line numbers with F8
nnoremap <F8> :set relativenumber!<cr>
" Yank entire file to clipboard
nnoremap <leader>y ggVG"+y
" Paste from clipboard
nnoremap <leader>p "+p
" Quit without saving with <leader>q
nnoremap <leader>q :q!<cr>
" Save and quit with <leader>w
nnoremap <leader>w :wq<cr>
" Custom mapping example
nnoremap W b
" Redo with Shift+U
nnoremap U <C-r>

" Other Vimscript Settings
" [Further settings and autocommands can be added here]

