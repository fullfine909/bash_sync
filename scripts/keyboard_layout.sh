#!/bin/zsh

# Set strict mode
set -euo pipefail

# Define constants
ORIGINAL_LAYOUT="/usr/share/X11/xkb/symbols/us"
BACKUP_LAYOUT="${ORIGINAL_LAYOUT}.backup"
USER_HOME=$(eval echo ~${SUDO_USER})
DOTS_DIR="${USER_HOME}/.dots"
CUSTOM_LAYOUT="${DOTS_DIR}/keyboard/custom_keyboard_layout/us"

# Function to backup the original layout
backup_original_layout() {
    if [[ ! -f "$BACKUP_LAYOUT" ]]; then
        echo "Backing up original US keyboard layout..."
        cp "$ORIGINAL_LAYOUT" "$BACKUP_LAYOUT"
        echo "Backup created at $BACKUP_LAYOUT"
    else
        echo "Backup already exists at $BACKUP_LAYOUT"
    fi
}

# Function to install the custom layout
install_custom_layout() {
    if [[ ! -f "$CUSTOM_LAYOUT" ]]; then
        echo "Error: Custom layout file not found at $CUSTOM_LAYOUT"
        exit 1
    fi
    echo "Installing custom keyboard layout..."
    cp "$CUSTOM_LAYOUT" "$ORIGINAL_LAYOUT"
    echo "Custom layout installed"
}

# Function to restore the original layout
restore_original_layout() {
    if [[ -f "$BACKUP_LAYOUT" ]]; then
        echo "Restoring original US keyboard layout..."
        cp "$BACKUP_LAYOUT" "$ORIGINAL_LAYOUT"
        echo "Original layout restored"
    else
        echo "Error: Backup file not found at $BACKUP_LAYOUT"
        exit 1
    fi
}

# Check if script is run as root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Main script
if [[ "$#" -eq 0 ]]; then
    echo "Usage: $0 [install|restore]"
    exit 1
fi

case "$1" in
    install)
        backup_original_layout
        install_custom_layout
        ;;
    restore)
        restore_original_layout
        ;;
    *)
        echo "Invalid option. Use 'install' or 'restore'."
        exit 1
        ;;
esac

echo "Operation completed. You may need to restart your X session or reboot for changes to take effect."
