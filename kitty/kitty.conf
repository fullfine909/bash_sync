# Kitty Terminal Configuration
# Visit https://sw.kovidgoyal.net/kitty/conf/ for more details
# Use `kitty +list-fonts` command to get a full list of supported fonts.

# vim:fileencoding=utf-8:foldmethod=marker
# DEFAULT MAPS

# Font Configuration
font_family Hack Nerd Bold
font_size 18.0

# Window Configuration
initial_window_width 640
initial_window_height 400
#dynamic_window_resize yes
window_border_width 0.5pt
window_resize_step_cells 2
window_resize_step_lines 2
enabled_layouts splits,stack,fat,tall,grid

# Include the splits configuration file
include splits.conf

# Layout configuration details can be found at
# https://sw.kovidgoyal.net/kitty/layouts/#the-splits-layout

# Scroll and Paste Configuration
scrollback_lines 8000
paste_actions quote-urls-at-prompt
strip_trailing_spaces never
show_hyperlink_targets yes

# Visual Window Selection Configuration
visual_window_select_characters 1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ

# Shell Integration Configuration
shell_integration enabled
confirm_os_window_close -1

# Mouse Configuration
mouse_map left click ungrabbed no-op
mouse_map ctrl+left click ungrabbed mouse_handle_click selection link prompt
mouse_map ctrl+left press ungrabbed mouse_selection normal
mouse_map right press ungrabbed copy_to_clipboard

# Tab Configuration
tab_bar_style powerline
tab_title_template "{fmt.fg.red}{bell_symbol}{activity_symbol}{fmt.fg.tab}{index}:{'🇿' if layout_name == 'stack' and num_windows > 1 else ''}{title}"

# Background Configuration
background_opacity 1.0
background_image none

# Remote Control Configuration
allow_remote_control yes
listen_on unix:$XDG_RUNTIME_DIR/kitty.sock
# Detailed documentation can be found at
# https://sw.kovidgoyal.net/kitty/conf/#opt-kitty.allow_remote_control

# Session Configuration
startup_session session.conf

# Hyperlink Configuration
allow_hyperlinks yes

# Modifier Key Configuration
kitty_mod ctrl+shift

# Key Mapping Configuration
include keymap.conf

# Decoration and Theme Configuration
hide_window_decorations yes
include themes/Monokai.conf

# Startup Session Configuration
startup_session ~/.config/kitty/session.conf
