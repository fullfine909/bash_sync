# ~/.dots/functions/midi.sh

# ===================================
# MIDI Functions
# ===================================

# Function to monitor MIDI controllers
midi_monitor() {
    # Check for required commands
    for cmd in aseqdump aconnect; do
        if ! command -v $cmd &> /dev/null; then
            echo "Error: $cmd is not installed. Please install alsa-utils package."
            return 1
        fi
    done

    # Function to list MIDI ports
    list_midi_ports() {
        echo "\nAvailable MIDI ports:"
        echo "-------------------"
        aconnect -i | grep client | while read -r line; do
            if [[ $line != *"Through"* ]] && [[ $line != *"System"* ]]; then
                local client_num=$(echo $line | grep -o 'client [0-9]*' | awk '{print $2}')
                local client_name=$(echo $line | sed 's/^[^"]*"//;s/"$//')
                echo "$client_num: $client_name"
            fi
        done
    }

    # Function to monitor MIDI events for a specific port
    monitor_midi_port() {
        local port=$1
        echo "\nMonitoring MIDI events from port $port..."
        echo "Press Ctrl+C to stop monitoring\n"

        # Start monitoring MIDI events
        aseqdump -p "$port" | while IFS= read -r line; do
            # Extract timestamp at the start of the line
            local timestamp=$(echo "$line" | grep -o '^\[ *[0-9.]*\]' | tr -d '[]')
            if [[ $line == *"Note on"* ]]; then
                local channel=$(echo "$line" | awk '/Note on/ {print $4}' | tr -d ',')
                local note=$(echo "$line" | grep -o 'note [0-9]*' | awk '{print $2}')
                local velocity=$(echo "$line" | grep -o 'velocity [0-9]*' | awk '{print $2}')
                echo "Note on  - Channel: $channel, Note: $note, Velocity: $velocity"

            elif [[ $line == *"Note off"* ]]; then
                local channel=$(echo "$line" | awk '/Note off/ {print $4}' | tr -d ',')
                local note=$(echo "$line" | grep -o 'note [0-9]*' | awk '{print $2}')
                local velocity=$(echo "$line" | grep -o 'velocity [0-9]*' | awk '{print $2}')
                echo "Note off - Channel: $channel, Note: $note, Velocity: $velocity"

            elif [[ $line == *"Control change"* ]]; then
                local channel=$(echo "$line" | awk '/Control change/ {print $4}' | tr -d ',')
                local control=$(echo "$line" | grep -o 'controller [0-9]*' | awk '{print $2}')
                local value=$(echo "$line" | grep -o 'value [0-9]*' | awk '{print $2}')
                echo "CC      - Channel: $channel, Control: $control, Value: $value"

            elif [[ $line == *"Pitch bend"* ]]; then
                local channel=$(echo "$line" | awk '/Pitch bend/ {print $4}' | tr -d ',')
                local value=$(echo "$line" | grep -o 'value [0-9]*' | awk '{print $2}')
                echo "Pitch   - Channel: $channel, Value: $value"
            fi
        done
    }

    # Main script
    while true; do
        list_midi_ports

        echo "\nEnter the port number to monitor (or 'q' to quit): "
        read choice

        if [[ $choice == "q" ]]; then
            echo "Exiting MIDI monitor."
            break
        fi

        # Validate input is a number
        if [[ $choice =~ ^[0-9]+$ ]]; then
            # Check if the port exists
            if aconnect -i | grep -q "client $choice"; then
                monitor_midi_port $choice
            else
                echo "Invalid port number. Please try again."
            fi
        else
            echo "Please enter a valid number or 'q' to quit."
        fi
    done
}

# Add command completion for midi_monitor
compdef _midi_monitor midi_monitor

# Command completion function
_midi_monitor() {
    _arguments \
        '1: :->ports' \
        '*: :->args'

    case $state in
        ports)
            local -a ports
            while read -r line; do
                if [[ $line != *"Through"* ]] && [[ $line != *"System"* ]]; then
                    local client_num=$(echo $line | grep -o 'client [0-9]*' | awk '{print $2}')
                    local client_name=$(echo $line | sed 's/^[^"]*"//;s/"$//')
                    ports+=("$client_num:$client_name")
                fi
            done < <(aconnect -i | grep client)
            _describe 'midi ports' ports
            ;;
    esac
}

# List MIDI Ports
list_midi_ports() {
    echo "\nAvailable MIDI ports:"
    echo "-------------------"
    aseqdump -l | while read -r line; do
        if [[ $line =~ ^[[:space:]]*[0-9]+:[0-9]+ ]] && \
           [[ ! $line =~ "System" ]] && \
           [[ ! $line =~ "Midi Through" ]]; then
            local port=$(echo "$line" | awk '{print $1}')
            local name=$(echo "$line" | awk '{for(i=2;i<=NF-1;i++)printf "%s ",$i;print $NF}' | sed 's/  *$//')
            local midi_name=$(echo "$line" | awk '{print $NF}')
            echo "'$name:$midi_name $port',"
        fi
    done
}

