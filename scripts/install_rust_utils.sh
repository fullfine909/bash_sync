#!/bin/bash

install_rust_utils() {
    local tools=("fd-find" "ripgrep" "bat" "eza" "locate")
    local installed_count=0
    local error_count=0

    echo "Checking and installing Rust utilities..."

    for tool in "${tools[@]}"; do
        if ! command -v "${tool//-find/}" &> /dev/null; then
            echo "Installing $tool..."
            if sudo dnf install -y "$tool"; then
                echo "$tool installed successfully."
                ((installed_count++))
            else
                echo "Error installing $tool."
                ((error_count++))
            fi
        else
            echo "$tool is already installed."
            ((installed_count++))
        fi
    done

    echo "Installation complete."
    echo "Installed/Already present: $installed_count"
    echo "Errors: $error_count"

}

# Run the function
install_rust_utils
