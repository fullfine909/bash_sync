# ~/.dots/functions/process.sh

# ===================================
# Process Management Functions
# ===================================

tpro() {
    local num_processes=${1:-5}
    ps aux --sort=-%mem | head -n "$((num_processes + 1))"
}

pss() {
    echo "USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND"
    ps aux | grep -v grep | grep "$1"
}

