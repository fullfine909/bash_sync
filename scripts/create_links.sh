#!/bin/bash

set -euo pipefail

DOTS_DIR="${HOME}/.dots"
BACKUP_DIR="${DOTS_DIR}/backup/config"
CONFIG_DIR="${DOTS_DIR}/config"

backup_and_link() {
    local file="$1"
    local home_file="${HOME}/${file}"
    local dots_file="${CONFIG_DIR}/${file}"
    local backup_file="${BACKUP_DIR}/${file}.$(date +%Y%m%d%H%M%S)"

    if [[ -e "${home_file}" && ! -L "${home_file}" ]]; then
        mkdir -p "${BACKUP_DIR}"
        mv "${home_file}" "${backup_file}"
        echo "Backed up ${home_file} to ${backup_file}"
    elif [[ -L "${home_file}" ]]; then
        echo "${home_file} is already a symlink. Removing..."
        rm "${home_file}"
    fi

    if [[ -e "${dots_file}" ]]; then
        ln -sf "${dots_file}" "${home_file}"
        echo "Created symlink: ${home_file} -> ${dots_file}"
    else
        echo "Warning: ${dots_file} not found in ${DOTS_DIR}"
    fi
}

echo "Starting dotfiles backup and symlink process"

files=('.vimrc' '.zshrc')

for file in "${files[@]}"; do
    echo "Processing ${file}"
    backup_and_link "${file}"
done

echo "Symlink creation complete"
echo "Script execution complete"
