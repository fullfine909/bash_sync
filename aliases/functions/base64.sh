# ~/.dots/functions/base64.sh

# ===================================
# Base64 Encoding/Decoding Functions
# ===================================

# ENCODE
e64() {
    local file=${1:-src/.env}
    local file_out=${2:-tmp_encode}
    base64 -w 0 "$file" > "$file_out"
}

# DECODE
d64() {
    local b64_text=$1
    local file=${2:-tmp_decode}
    echo "$b64_text" | base64 -d > "$file"
}

