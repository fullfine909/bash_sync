#!/bin/bash

set -euo pipefail

log() {
    echo "[$(date +'%Y-%m-%d %H:%M:%S')] $1"
}

handle_error() {
    log "Error occurred on line $1"
    exit 1
}

trap 'handle_error $LINENO' ERR

install_or_update_kitty() {
    if command -v kitty &> /dev/null; then
        log "Updating Kitty..."
        sudo dnf update -y kitty
    else
        log "Installing Kitty..."
        sudo dnf install -y kitty
    fi
}

verify_installation() {
    if command -v kitty &> /dev/null; then
        local version=$(kitty --version)
        log "Kitty $version has been successfully installed/updated."
    else
        log "Error: Kitty installation failed."
        exit 1
    fi
}

setup_symlink() {
    local config_path="$HOME/.config/kitty"
    local dots_kitty_path="$HOME/.dots/kitty"
    
    if [ -d "$config_path" ]; then
        log "Removing existing ~/.config/kitty directory..."
        rm -rf "$config_path"
    fi

    log "Creating symlink for Kitty configuration..."
    ln -s "$dots_kitty_path" "$config_path"
    log "Symlink created: $config_path -> $dots_kitty_path"
}

main() {
    log "Starting Kitty installation/update process..."
    
    log "Updating system packages..."
    sudo dnf update -y
    
    install_or_update_kitty
    verify_installation
    
    setup_symlink
    
    log "Kitty installation/update process completed."
}

main

