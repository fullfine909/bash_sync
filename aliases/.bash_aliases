#!/bin/sh

# ===================================
# Define the aliases folder
# ===================================
ALIASES_FOLDER="$HOME/.dots/aliases"
FUNCTIONS_FOLDER="$HOME/.dots/aliases/functions"


# ===================================
# MAIN ALIAS FILE
# ===================================
alias ere="vi $ALIASES_FOLDER/.bash_aliases"
alias err="source $ALIASES_FOLDER/.bash_aliases"

# ===================================
# MAIN ALIASES
# ===================================
alias erba="vi $ALIASES_FOLDER/.bash_basics"
alias rrba="source $ALIASES_FOLDER/.bash_basics"

alias erdo="vi $ALIASES_FOLDER/.bash_docker"
alias rrdo="source $ALIASES_FOLDER/.bash_docker"

alias ergi="vi $ALIASES_FOLDER/.bash_git"
alias rrgi="source $ALIASES_FOLDER/.bash_git"

alias erpy="vi $ALIASES_FOLDER/.bash_python"
alias rrpy="source $ALIASES_FOLDER/.bash_python"

alias eraw="vi $ALIASES_FOLDER/.bash_aws"
alias rraw="source $ALIASES_FOLDER/.bash_aws"

alias erss="vi $ALIASES_FOLDER/.bash_services"
alias rrss="source $ALIASES_FOLDER/.bash_services"

alias erru="vi $ALIASES_FOLDER/.bash_rust"
alias rrru="source $ALIASES_FOLDER/.bash_rust"

alias ertt="vi $ALIASES_FOLDER/.bash_ttg"
alias rrtt="source $ALIASES_FOLDER/.bash_ttg"

# ===================================
# LOCAL ALIASES
# ===================================
alias erlo="vi $ALIASES_FOLDER/.bash_local"
alias rrlo="source $ALIASES_FOLDER/.bash_local"

# ===================================
# FEDORA SPECIFIC ALIASES
# ===================================
if [ -z "$MSYSTEM" ]; then
  alias eros="vi $ALIASES_FOLDER/.bash_os"
  alias rros="source $ALIASES_FOLDER/.bash_os"
  source $ALIASES_FOLDER/.bash_os
fi

# ===================================
# FUNCTIONS ALIASES
# ===================================
alias erbase64="vi $FUNCTIONS_FOLDER/base64.sh"
alias rrbase64="source $FUNCTIONS_FOLDER/base64.sh"

alias ermidi="vi $FUNCTIONS_FOLDER/midi.sh"
alias rrmidi="source $FUNCTIONS_FOLDER/midi.sh"

alias erprocess="vi $FUNCTIONS_FOLDER/process.sh"
alias rrprocess="source $FUNCTIONS_FOLDER/process.sh"

alias ersutils="vi $FUNCTIONS_FOLDER/search_utils.sh"
alias rrsutils="source $FUNCTIONS_FOLDER/search_utils.sh"

alias erwindsurf="vi $FUNCTIONS_FOLDER/windsurf.sh"
alias rrwindsurf="source $FUNCTIONS_FOLDER/windsurf.sh"


# ===================================
# EXTRA CONFIGS
# ===================================
alias ervi="vi ~/.vimrc"
alias rrvi="source ~/.vimrc"

alias erki="cd ~/.config/kitty"
alias erks="vi ~/.config/kitty/session.conf"

alias erz="vi ~/.zshrc"
alias rrz="source ~/.zshrc"


# ===================================
# ACTIVATE EXTRA ALIASES
# ===================================
rrba
rrdo
rrgi
rrpy
rraw
rrss
rrru
rrtt
rrlo
# Functions
rrbase64
rrmidi
rrprocess
rrsutils
rrwindsurf

# ===================================
# PRIVATE ENVIRONMENT VARIABLES
# ===================================
source $ALIASES_FOLDER/.private


# ===================================
# FUNCTIONS
# ===================================
# Pulls latest changes in ~/.dots repository without changing the current working directory
ergpl() {
  curr_dir=$(pwd)
  cd ~/.dots
  gpl
  cd "$curr_dir"
}

