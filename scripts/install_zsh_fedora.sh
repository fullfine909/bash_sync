#!/bin/sh

# This script installs and configures Zsh and Oh My Zsh on a Fedora system.
# It checks for existing installations to avoid unnecessary actions.

echo "Starting Zsh installation and configuration process..."

# Update the system packages
echo "Updating system packages..."
sudo dnf update -y

# Check if Zsh is already installed
if ! command -v zsh &> /dev/null
then
    echo "Zsh is not installed. Installing Zsh..."
    sudo dnf install -y zsh
else
    echo "Zsh is already installed. Skipping installation."
fi

# Check if Oh My Zsh is already installed
if [ ! -d "~/.oh-my-zsh" ]
then
    echo "Oh My Zsh is not installed. Installing Oh My Zsh..."
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
else
    echo "Oh My Zsh is already installed. Skipping installation."
fi

# Install Powerlevel10k theme
echo "Installing Powerlevel10k theme..."
if [ ! -d "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k" ]; then
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
else
    echo "Powerlevel10k theme is already installed. Skipping installation."
fi


# Install or update Zsh plugins
echo "Installing/Updating Zsh plugins (autosuggestions and syntax-highlighting)..."
sudo dnf install -y zsh-autosuggestions zsh-syntax-highlighting

# Configure Zsh by adding plugin source lines to .zshrc
echo "Configuring Zsh..."
if ! grep -q "zsh-autosuggestions" ~/.zshrc
then
    echo "Adding zsh-autosuggestions to .zshrc..."
    echo "source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh" >> ~/.zshrc
else
    echo "zsh-autosuggestions already configured in .zshrc. Skipping."
fi

if ! grep -q "zsh-syntax-highlighting" ~/.zshrc
then
    echo "Adding zsh-syntax-highlighting to .zshrc..."
    echo "source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ~/.zshrc
else
    echo "zsh-syntax-highlighting already configured in .zshrc. Skipping."
fi

# Change the default shell to Zsh if it's not already
if [[ $SHELL != "/usr/bin/zsh" ]]
then
    echo "Changing default shell to Zsh..."
    chsh -s $(which zsh)
    echo "Default shell changed to Zsh. Please log out and log back in for changes to take effect."
else
    echo "Zsh is already the default shell. No change needed."
fi

echo "Script execution complete. Zsh environment setup finished."
echo "If you made any changes, remember to log out and log back in to see the effects."
echo "Enjoy your Zsh experience!"
